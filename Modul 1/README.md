# Modul 1 - Praktikum Pemrograman Berorientasi Objek

## Tools
- [JDoodle](https://www.jdoodle.com/online-java-compiler/)
- [Programmiz](https://www.programiz.com/java-programming/online-compiler/)

## Contoh Kasus 1
```java
class Manusia {
    String nama;
    int umur;
    int uang;
    
    void bilang(String kata) {
        System.out.println(this.nama + " : " + kata);
    }
    
    void pinjemUang (Manusia manusia, int uangDipinjem) {
        
        this.bilang(manusia.nama + ", pinjem uang " + uangDipinjem + " dong");
        if(manusia.uang <= uangDipinjem) {
            manusia.bilang("Ga ada uang segitu mah, kumaha ?\n");
        } else {

            // a += b artinya a = a + b
            // a -= b artinya a = a - b

            manusia.uang -= uangDipinjem;
            this.uang += uangDipinjem;
            manusia.bilang("Nih saya pinjami, mudah2an kamu ada rezeki untuk mengembalikan\n");
        }
    }
}

class SimulasiPinjemUang {
    public static void main(String[] args) {
        
        Manusia m1 = new Manusia();
        m1.nama = "insan";
        m1.uang = 15000;
        
        Manusia m2 = new Manusia();
        m2.nama = "ica";
        m2.uang = 20000000;
        
        m1.pinjemUang(m2, 80000000);
    }
}
```

## Contoh Kasus 2
```java
// Online Java Compiler
// Use this editor to write, compile and run your Java code online

class Kompor {
    String nama;
    boolean nyala = false;
    
    void nyalakan() {
        this.nyala = true;
        System.out.println("Kompor dinyalakan");
    }
    
    void matikan() {
        this.nyala = false;
        System.out.println("Kompor dimatikan");
    }
}

class Ruangan {
    String nama;
    Kompor kompor;
}

class Rumah {
    String alamat;
    Ruangan dapur;
    Ruangan kamarTidur;
    Ruangan kamarMandi;
}

class Warga {
    String nama;
    Rumah rumah;
}

class Latihan {
    public static void main(String[] args) {
        
        Warga warga = new Warga();
        warga.nama = "Aldrian";
        
        Rumah rumah = new Rumah();
        Ruangan dapur = new Ruangan();
        Kompor kompor = new Kompor();
        
        dapur.kompor = kompor;
        rumah.dapur = dapur;
        warga.rumah = rumah;
        
        warga.rumah.dapur.kompor.nyalakan();
        
        warga.rumah.dapur.kompor.matikan();
        
    }
}
```

## Challange
Buatlah simulasi peristiwa menggunakan OOP. Pilih tema:
1. Penduduk yang buang sampah sembarangan hingga kali meluap dan banjir
2. Selebgram yang lebih banyak direport daripada dilike oleh sesama pengguna
3. Manusia yang punya poin amal baik dan buruk yang bertambah dan berkurang karena tindakannya
4. Penjual bacang yang mendengar suara di udara, bisa dari mesjid, dari warga, atau dari klakson mobil
5. Warga penghasilan rendah kemampuannya tidak bertambah jika hanya diberi pelatihan skill, harus dimotivasi juga
6. Simulasi lain improvisasi kamu sendiri
